import asyncHandler from "express-async-handler";

import Story from "../model/storyModel.js";

/**
 * @desc    Get all stories
 * @route   GET  /api/stories
 * @access public
 */

const getStories = asyncHandler(async (req, res) => {
  const stories = await Story.find({});

  if (Array.isArray(stories)) {
    const categoryListArr = [
      ...new Set(stories.flatMap((story) => story.type)),
    ];

    res.json({ stories, categoryListArr });
  } else {
    res.status(500).json({ message: "Error fetching stories" });
  }
});

/**
 * @desc     Get single Story
 * @route    GET /api/stories/:id
 * @access public
 */
const getStoryById = asyncHandler(async (req, res) => {
  try {
    const story = await Story.findById(req.params.id);
    if (story) {
      res.json(story);
    } else {
      res.status(404).json({ message: "story not found" });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

/**
 * @desc		Get logged in user's stories
 * @route		GET /api/stories/mystories
 * @access	private
 */
const getMyStories = asyncHandler(async (req, res) => {
  const stories = await Story.find({ belongsTo: req.user._id });
  res.json(stories);
});

/**
 * @desc		Create new story
 * @route		POST /api/stories
 * @access	private
 */
const createStory = asyncHandler(async (req, res) => {
  const { heading, type, description, image, imageArray, likes } = req.body;

  const story = new Story({
    heading,
    type,
    description,
    image,
    imageArray,
    likes,
    belongsTo: req.user._id,
  });

  const createdStory = await story.save();
  res.status(201).json(createdStory);
});

/**
 * @desc Like a story
 * @route POST /api/stories/like
 * @access private
 */
const likeStory = asyncHandler(async (req, res) => {
  const { userId, storyId } = req.body;

  // Find the story by ID
  const story = await Story.findById(storyId);

  if (!story) {
    res.status(404).json({ message: "Story not found" });
    return;
  }

  // Check if the user has already liked the story (optional)
  if (story.likes.includes(userId)) {
    res.status(400).json({ message: "User already liked the story" });
    return;
  }

  // Append the userId to the likes array
  story.likes.push(userId);

  // Save the updated story
  const updatedStory = await story.save();

  res.status(200).json({ message: "Story liked successfully", updatedStory });
});

/**
 * @desc Unlike a story
 * @route POST /api/stories/unlike
 * @access private
 */

const unlikeStory = asyncHandler(async (req, res) => {
  const { userId, storyId } = req.body;

  // Find the story by ID
  const story = await Story.findById(storyId);

  if (!story) {
    res.status(404).json({ message: "Story not found" });
    return;
  }

  // Check if the user has liked the story
  const likedIndex = story.likes.indexOf(userId);
  if (likedIndex === -1) {
    res.status(400).json({ message: "User has not liked the story" });
    return;
  }

  // Remove the userId from the likes array
  story.likes.splice(likedIndex, 1);

  // Save the updated story
  const updatedStory = await story.save();

  res.status(200).json({ message: "Story unliked successfully", updatedStory });
});

export {
  getStories,
  getStoryById,
  getMyStories,
  createStory,
  likeStory,
  unlikeStory,
};

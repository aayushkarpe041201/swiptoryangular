import asyncHandler from "express-async-handler";

import User from "../model/userModel.js";
import generateToken from "../utils/generateToken.js";

/**
 * @description     Auth user
 * @router          POST /api/users/login
 * @access          public
 */

const authUser = asyncHandler(async (req, res) => {
  const { email, password } = req.body;

  const user = await User.findOne({ email });

  if (user && (await user.matchPassword(password))) {
    res.json({
      _id: user.id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      bookmarks: user.bookmarks,
      token: generateToken(user._id),
    });
  } else {
    res.status(401);
    throw new Error("Envalid email or password");
  }
});

/**
 * @description     Register new user
 * @route           Post  /api/users
 * @access          public
 */

const registerUser = asyncHandler(async (req, res) => {
  const { name, email, password } = req.body;

  const userExists = await User.findOne({ email });

  if (userExists) {
    res.status(400); // bad request
    throw new Error("User already exists");
  }

  const user = await User.create({ name, email, password });

  if (user) {
    // successfull created
    res.status(201).json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
      bookmarks: user.bookmarks,
      token: generateToken(user._id),
    });
  } else {
    res.status(400); //Bad request
    throw new Error("Invalid user data");
  }
});

/**
 * @description     GET all users
 * @route           GET /api/users
 * @access          private/admin
 */

const getUsers = asyncHandler(async (req, res) => {
  const users = await User.find({}).select("-password");
  res.json(users);
});

/**
 * @description Delete user
 * @route       DELETE  /api/users/:id
 * @access      private/admin
 */

const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  if (user) {
    await user.remove();
    res.json({ message: "User delete" });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

/**
 * @description     Get user by Id
 * @route           GET /api/users/:id
 * @access          private/admin
 */

const getUserById = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id).select("-password");

  if (user) {
    res.json(user);
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

/**
 * @desc		Get user profile
 * @route		POST /api/users/profile
 * @access	private
 */
const getUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    res.json({
      _id: user._id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

/**
 * @desc		Update user profile
 * @route		PUT /api/users/profile
 * @access	private
 */
const updateUserProfile = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    if (req.body.password) {
      user.password = req.body.password;
    }

    const updatedUser = await user.save();

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
      token: generateToken(updatedUser._id),
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

/**
 * @desc		Update a user
 * @route		PUT /api/users/:id
 * @access	private/admin
 */

const updateUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.params.id);

  if (user) {
    user.name = req.body.name || user.name;
    user.email = req.body.email || user.email;
    user.isAdmin = req.body.isAdmin;

    const updatedUser = await user.save();

    res.json({
      _id: updatedUser._id,
      name: updatedUser.name,
      email: updatedUser.email,
      isAdmin: updatedUser.isAdmin,
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

/**
 * @desc		Bookmark a story
 * @route		POST /api/users/bookmark
 * @access	private
 */
const bookmarkStory = async (req, res) => {
  const { userId, storyId } = req.body;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check if the story is already bookmarked (optional)
    if (user.bookmarks.includes(storyId)) {
      return res.status(400).json({ message: "Story already bookmarked" });
    }

    // Append the storyId to the bookmarks array
    user.bookmarks.push(storyId);

    // Save the updated user
    const updatedUser = await user.save();

    if (updatedUser) {
      res.status(200).json({
        _id: updatedUser.id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        bookmarks: updatedUser.bookmarks,
        token: generateToken(updatedUser._id),
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

/**
 * @desc		Unbookmark a story
 * @route		POST /api/users/unbookmark
 * @access	private
 */
const unbookmarkStory = async (req, res) => {
  const { userId, storyId } = req.body;

  try {
    // Find the user by ID
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Check if the story is bookmarked
    const bookmarkIndex = user.bookmarks.indexOf(storyId);
    if (bookmarkIndex === -1) {
      return res.status(400).json({ message: "Story is not bookmarked" });
    }

    // Remove the storyId from the bookmarks array
    user.bookmarks.splice(bookmarkIndex, 1);

    // Save the updated user
    const updatedUser = await user.save();

    if (updatedUser) {
      res.status(200).json({
        _id: updatedUser.id,
        name: updatedUser.name,
        email: updatedUser.email,
        isAdmin: updatedUser.isAdmin,
        bookmarks: updatedUser.bookmarks,
        token: generateToken(updatedUser._id),
      });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export {
  authUser,
  getUserById,
  getUsers,
  registerUser,
  deleteUser,
  getUserProfile,
  updateUserProfile,
  updateUser,
  bookmarkStory,
  unbookmarkStory,
};

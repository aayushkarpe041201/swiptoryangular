import mongoose from "mongoose";

const storySchema = new mongoose.Schema({
  heading: {
    type: String,
    required: true,
  },
  type: {
    type: [String],
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
  imageArray: {
    type: [String],
    required: true,
  },
  belongsTo: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
    unique: false,
  },
  likes: {
    type: [mongoose.Schema.Types.ObjectId],
    required: true,
  },
});

const Story = mongoose.model("Story", storySchema);

export default Story;

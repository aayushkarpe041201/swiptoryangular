import express from "express";

import {
  getStories,
  getStoryById,
  getMyStories,
  createStory,
  likeStory,
  unlikeStory,
} from "../controllers/storyControllers.js";

import { protect } from "../middlewares/authMiddleware.js";

const router = express.Router();

router.route("/").get(getStories);
router.route("/:id").get(getStoryById);
router.route("/createStory").post(protect, createStory);
router.route("/mystories").get(protect, getMyStories);
router.route("/like").post(likeStory);
router.route("/unlike").post(unlikeStory);

export default router;

const stories = [
  {
    heading: "Indian food",
    type: ["food", "india"],
    description:
      "Inspirational designs, illustrations, and graphic elements from the world's best designers.",
    image:
      "https://imgs.search.brave.com/ayNK9ltrLU_jyaqj_1nUv8KU3plesjgtE0Rz3fiAOLM/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9jNC53/YWxscGFwZXJmbGFy/ZS5jb20vd2FsbHBh/cGVyLzYyLzU2NC85/MzkvY3Vpc2luZS1m/b29kLWluZGlhLWlu/ZGlhbi13YWxscGFw/ZXItcHJldmlldy5q/cGc",
    imageArray: [
      "https://imgs.search.brave.com/ayNK9ltrLU_jyaqj_1nUv8KU3plesjgtE0Rz3fiAOLM/rs:fit:860:0:0/g:ce/aHR0cHM6Ly9jNC53/YWxscGFwZXJmbGFy/ZS5jb20vd2FsbHBh/cGVyLzYyLzU2NC85/MzkvY3Vpc2luZS1m/b29kLWluZGlhLWlu/ZGlhbi13YWxscGFw/ZXItcHJldmlldy5q/cGc",
    ],
    likes: [],
  },
  {
    heading: "Travel Adventures",
    type: ["medical", "fruits"],
    description:
      "Explore breathtaking destinations and embark on thrilling adventures.",
    image:
      "https://imgs.search.brave.com/fKgOFCEarzywt6Z0f_rdzZcjgSOtQEG3pKamPcICsNA/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWcu/ZnJlZXBpay5jb20v/cGhvdG9zLXByZW1p/dW0vY29sbGVjdGlv/bi1mcnVpdHMtZm9u/ZC1hbGltZW50YWly/ZS1mb3JtYXQtcG9y/dHJhaXQtcG9tbWVz/LW9yYW5nZXMtY2l0/cm9ucy1mcnVpdHMt/ZnJhaXNfNzcwMTIz/LTM1NjUuanBnP3Np/emU9NjI2JmV4dD1q/cGc",
    imageArray: [
      "https://imgs.search.brave.com/fKgOFCEarzywt6Z0f_rdzZcjgSOtQEG3pKamPcICsNA/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWcu/ZnJlZXBpay5jb20v/cGhvdG9zLXByZW1p/dW0vY29sbGVjdGlv/bi1mcnVpdHMtZm9u/ZC1hbGltZW50YWly/ZS1mb3JtYXQtcG9y/dHJhaXQtcG9tbWVz/LW9yYW5nZXMtY2l0/cm9ucy1mcnVpdHMt/ZnJhaXNfNzcwMTIz/LTM1NjUuanBnP3Np/emU9NjI2JmV4dD1q/cGc",
    ],
    likes: [],
  },
  {
    heading: "Tech Innovations",
    type: ["healthcare", "fruits"],
    description:
      "Discover the latest technological innovations shaping the future.",
    image:
      "https://imgs.search.brave.com/WZXDsZfJ2yuDH92pgI1biSxtNIE5sxgJeMvD604yDQ0/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWcu/ZnJlZXBpay5jb20v/ZnJlZS1waG90by9j/bG9zZS11cC1hcHBs/ZS1zdGlsbC1saWZl/XzIzLTIxNTA3MTMy/NjYuanBnP3NpemU9/NjI2JmV4dD1qcGc",
    imageArray: [
      "https://imgs.search.brave.com/WZXDsZfJ2yuDH92pgI1biSxtNIE5sxgJeMvD604yDQ0/rs:fit:500:0:0/g:ce/aHR0cHM6Ly9pbWcu/ZnJlZXBpay5jb20v/ZnJlZS1waG90by9j/bG9zZS11cC1hcHBs/ZS1zdGlsbC1saWZl/XzIzLTIxNTA3MTMy/NjYuanBnP3NpemU9/NjI2JmV4dD1qcGc",
    ],
    likes: [],
  },
];

export default stories;
